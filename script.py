from docxtpl import DocxTemplate
from docx2pdf import convert
import time
with open("data.csv","r") as csvf:
    op = csvf.readlines()

print(type(op))

for i in op[1:]:
    line = i.split(";")
    print(line)
    doc = DocxTemplate("template.docx")
    context = {"order_date": line[4],"order_num": line[5],"FIO": line[1] + " " +line[2] + " " + line[3],"form_unit": line[6],"direction": line[7]}
    doc.render(context)
    doc.save("output/word/" + line[0] + "_" + line[5][:-2] + ".docx")
    time.sleep(2)
    convert("output/word/" + line[0] + "_" + line[5][:-2] + ".docx", "output/pdf/" + line[0] + "_" + line[5][:-2] + ".pdf")
print("done")
